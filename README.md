# TheHaywireHax
A collection of scripts and hacks I made. Enjoy!!!

# Scripts
## GitUpdater
> A script that searches for all the git repos and starts a `git pull`.  

`$ ./gitupdater.sh`

## CommandMaker
> This helps to create commands just by making a `*.sh` executable and copying it to `/usr/bin/`.  

`./commandmaker.sh INPUTFILE COMMANDNAME`

## Search
> Searches for a given keyword.  
**See UsefulAliasses**

## UsefulAliasses
> A list of useful aliasses.  
***Put in ~/.bashrc***

## Webrecon
> A toolset created for automatic website scanning.

```
$ cd webrecon
$ ./install.sh
$ ./webrecon.sh -h

```
### Included Tools
- dnsrecon
- spoofcheck
- sublist3r
- certspotter
- knockpy
- wfuzz
- nikto
- arjun.py
- burpsuite

### Stages
- subdomain discovery
- subdirectory bruteforce
- parameter bruteforce
